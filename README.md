# GyroCo.de

Open http://gyroco.de on a mobile device and grant motion permissions.   
Spin the device to view the image stabilized.  
Add to Home screen on iOS or click Fullscreen on Android for more screen space.  

## Phone Harnesses

Look in the /models directory for your particular phone's harness 3D model to print.  
Submit an issue for a model you'd like to see added.  
Submit a merge request with new models if you want to contribute your own.  



## TODO

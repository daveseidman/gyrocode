import React from 'react';
import ReactDOM from 'react-dom/client';
import inobonce from 'inobounce'
import App from './App';

ReactDOM.createRoot(document.getElementById('root')).render(
  <App />,
);

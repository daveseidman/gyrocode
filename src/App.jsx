
import './index.scss';
import React, { useState, useEffect } from 'react';
import compassImage from './assets/compass.png'

const App = () => {
  const [gyroEnabled, setGyroEnabled] = useState(false);
  const [rotation, setRotation] = useState(0);

  const handleOrientation = (e) => {
    if (gyroEnabled) setRotation(e.alpha)
  }

  useEffect(() => {
    if (gyroEnabled)
      addEventListener('deviceorientation', handleOrientation, false);
    else {
      removeEventListener('deviceorientation', handleOrientation, false);
    }

  }, [gyroEnabled])

  const enableGyro = () => {
    if (typeof (DeviceMotionEvent) !== 'undefined' && typeof (DeviceMotionEvent.requestPermission) === 'function') {
      DeviceMotionEvent.requestPermission()
        .then(permissionState => {
          if (permissionState === 'granted') {
            setGyroEnabled(true);
          } else {
            setGyroEnabled(false);
          }
        })
        .catch(error => {
          setGyroEnabled(false);
          console.error('Error requesting gyroscope permission:', error);
        });
    } else {
      // Gyroscope API not supported in this browser.
      console.log('Gyroscope API is not supported in this browser.');
    }
  }

  const disableGyro = () => {
    setGyroEnabled(false);
  }



  return (
    <div className="app">
      <h1>Gyrocode</h1>
      <div className='gyro'>
        <img
          className="gyro-image"
          src={compassImage}
          style={{ transform: `rotate(${rotation}deg)` }}
        >
        </img>
      </div>
      <div className='controls'>
        <button className={gyroEnabled ? 'hidden' : ''} type="button" onClick={enableGyro}>Enable Gyro</button>
        <button className={gyroEnabled ? '' : 'hidden'} type="button" onClick={disableGyro}>Disable Gyro</button>
      </div>
      <div className="footer">
        <p>A Digital Stunt by <a href="https://daveseidman.com" target="_blank">Dave Seidman</a></p>
      </div>
    </div>
  )
}
export default App;